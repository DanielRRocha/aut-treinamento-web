# language : pt

@Login
Funcionalidade: Login

  @LoginComSucesso @Login
  Cenário: Realizar login com sucesso
    Dado Que eu esteja na tela de login
    Quando Faço login com o usuario "standard_user" e senha "secret_sauce"
    Então Sou autenticado com sucesso

  @LoginInvalido @Login
  Cenário: Tentativa de Login com email inválido
    Dado Que eu esteja na tela de login
    Quando Faço login com o usuario "teste" e senha "demoadmin"
    Então é apresentada a mensagem "Epic sadface: Username and password do not match any user in this service"
