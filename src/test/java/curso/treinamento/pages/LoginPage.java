package curso.treinamento.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import curso.treinamento.utils.Helper;

public class LoginPage extends Helper {

	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//span[text()='Remove Frame']")
	private WebElement linkRemoveFrame;
	
	@FindBy(id = "user-name")
	private WebElement campoUsername;

	@FindBy(xpath = "//input[@type='password']")
	private WebElement campoPassword;

	@FindBy(id = "login-button")
	private WebElement botaoLogin;

	@FindBy(xpath = "//h3[text()='Username and password do not match any user in this service']")
	private WebElement textoErro;

	public void clicar_remove_frame() {
		if(Helper.elemento_existe(linkRemoveFrame, 10)) {
			linkRemoveFrame.click();
		}
	}
	
	public Boolean validar_tela_login() {
		screenshot("Tela Login");
		return elemento_existe(botaoLogin, 30);
	}

	public void preencher_username(String userName) {
		campoUsername.sendKeys(userName);
	}

	public void preencher_password(String password) {
		campoPassword.sendKeys(password);
		screenshot("Login preenchido");
	}

	public void clicar_botao_login() {
		botaoLogin.click();
	}

	public Boolean validar_login_invalido(String erro) throws InterruptedException {
		aguardar_elemento(10, textoErro);
		screenshot("Mensagem de Login Invalidado Apresentada");
		return erro.equals(textoErro.getText());
	}
	
}
