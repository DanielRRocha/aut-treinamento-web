package curso.treinamento.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import curso.treinamento.utils.Helper;

public class CarrinhoPage extends Helper {

	public CarrinhoPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='Your Cart']")
	private WebElement textoYourCart;
	
	@FindBy(xpath = "//button[text()='REMOVE']")
	private WebElement botaoRemove;
	
	@FindBy(xpath = "//a[@class='btn_action checkout_button']")
	private WebElement botaoCheckout;
	

	public Boolean validar_pagina_carrinho() {
		aguardar_elemento(10, textoYourCart);
		screenshot("Tela Carrinho de Compras");
		return textoYourCart.isDisplayed();
	}
	
	public Boolean validar_item_no_carrinho() {
		return botaoCheckout.isDisplayed();
	}
	
	public void clicar_checkout() {
		botaoCheckout.click();
	}
}
