package curso.treinamento.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import curso.treinamento.setup.Hooks;
import curso.treinamento.utils.Helper;
import org.junit.Assert;

public class OverviewPage extends Helper {

	public OverviewPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='Checkout: Overview']")
	private WebElement textoOverview;
	
	@FindBy(xpath = "//div[@class='cart_quantity_label']")
	private WebElement textoQty;
	
	@FindBy(xpath = "//div[text()='Payment Information:']")
	private WebElement textoPayment;
	
	@FindBy(xpath = "//div[text()='Shipping Information:']")
	private WebElement textoShipping;
	
	@FindBy(xpath = "//div[text()='Total: $']")
	private WebElement textoTotal;
	
	@FindBy(xpath = "//a[text()='CANCEL']")
	private WebElement botaoCancelar;
	
	@FindBy(xpath = "//a[text()='FINISH']")
	private WebElement botaoFinish;
	
	public Boolean validar_tela_overview() {
		aguardar_elemento(10, textoOverview);
		screenshot("Tela Overview");
		return textoOverview.isDisplayed();
	}
	
	public void validar_itens_tela_overview() {
		Assert.assertTrue("Texto Payment Information não encontrado", textoPayment.isDisplayed());
		Assert.assertTrue("Texto Shipping Information não encontrado", textoShipping.isDisplayed());
		Assert.assertTrue("Texto Total não encontrado", textoTotal.isDisplayed());
		
		JavascriptExecutor js = (JavascriptExecutor) Hooks.getDriver();
		js.executeScript("arguments[0].scrollIntoView();", textoQty);

		screenshot("Tela Overview - Segunda parte");
		Assert.assertTrue("Botão Cancelar não encontrado", botaoCancelar.isDisplayed());
		Assert.assertTrue("Botão Finish não encontrado", botaoFinish.isDisplayed());
	}
	
	public void clicar_botao_finish() {
		botaoFinish.click();
	}

}
