package curso.treinamento.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import curso.treinamento.utils.Helper;

public class YourInformationPage extends Helper {

	public YourInformationPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "first-name")
	private WebElement firstName;
	
	@FindBy(id = "last-name")
	private WebElement lastName;
	
	@FindBy(id = "postal-code")
	private WebElement postalCode;
	
	@FindBy(xpath = "//input[@class='btn_primary cart_button']")
	private WebElement botaoContinue;
	

	public Boolean validar_pagina_your_information() {
		aguardar_elemento(10, firstName);
		screenshot("Tela Carrinho de Compras");
		return firstName.isDisplayed();
	}
	
	public void preencher_dados() {
		firstName.sendKeys("Daniel");
		lastName.sendKeys("Rocha");
		postalCode.sendKeys("04579200");
	}
	
	public void clicar_continue() {
		botaoContinue.click();
	}
}
