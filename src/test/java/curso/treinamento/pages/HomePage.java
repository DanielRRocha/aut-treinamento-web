package curso.treinamento.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import curso.treinamento.setup.Hooks;
import curso.treinamento.utils.Helper;

public class HomePage extends Helper{

	public HomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//strong[contains(text(), 'Logout')]")
	private WebElement linkLogout;
	
	@FindBy(xpath = "//div[@class='product_label']")
	private WebElement textoProdutos;
	
	@FindBy(xpath = "//button[@class='btn_primary btn_inventory']")
	private WebElement botaoComprar;
	
	@FindBy(xpath = "//span[@class='fa-layers-counter shopping_cart_badge']")
	private WebElement botaoCarrinho;
	
	public Boolean validar_pagina() {
		aguardar_elemento(30, textoProdutos);
		screenshot("Home Apresentada");
		return textoProdutos.isDisplayed();
	}
	
	public void selecionar_produto(String item) {
		WebElement produto = Hooks.getDriver().findElement(By.xpath("//div[text()='" + item + "']"));
		produto.click();
	}
	
	public boolean validar_pagina_produto(String item) {
		WebElement produto = Hooks.getDriver().findElement(By.xpath("//div[text()='" + item + "']"));
		aguardar_elemento(10, produto);
		screenshot("Tela do Produto " + item + "Apresentada");
		return produto.isDisplayed();
	}
	
	public void clicar_comprar_produto() {
		aguardar_elemento(10, botaoComprar);
		botaoComprar.click();
	}
	
	public void clicar_carrinho_compras() {
		aguardar_elemento(10, botaoCarrinho);
		botaoCarrinho.click();
	}
}
