# language : pt

@Login
Funcionalidade: Produtos

  @ComprarCamiseta @Produtos
  Cenário: Comprar camiseta com sucesso
    Dado Que eu esteja na tela de produtos com usuario "standard_user" e senha "secret_sauce"
    Quando Compro um item "Sauce Labs Bolt T-Shirt"
    Então Confirmo que compra foi efetuada

  