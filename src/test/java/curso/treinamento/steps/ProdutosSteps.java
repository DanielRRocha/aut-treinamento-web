package curso.treinamento.steps;

import org.junit.Assert;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import curso.treinamento.pages.CarrinhoPage;
import curso.treinamento.pages.FinishPage;
import curso.treinamento.pages.HomePage;
import curso.treinamento.pages.LoginPage;
import curso.treinamento.pages.OverviewPage;
import curso.treinamento.pages.YourInformationPage;
import curso.treinamento.setup.Hooks;

public class ProdutosSteps {

	private LoginPage 			loginPage 			= 	new LoginPage(Hooks.getDriver());
	private HomePage 			homePage 			= 	new HomePage(Hooks.getDriver());
	private CarrinhoPage		carrinhoPage 		=	new CarrinhoPage(Hooks.getDriver());
	private YourInformationPage	yourInformationPage	=	new YourInformationPage(Hooks.getDriver());
	private OverviewPage		overviewPage		=	new OverviewPage(Hooks.getDriver());
	private FinishPage			finishPage			=	new FinishPage(Hooks.getDriver());

	@Dado("Que eu esteja na tela de produtos com usuario {string} e senha {string}")
	public void que_eu_esteja_na_tela_de_produtos_com_usuario_e_senha (String user, String pass) throws InterruptedException {
		loginPage.preencher_username(user);
		loginPage.preencher_password(pass);
		loginPage.clicar_botao_login();
		Assert.assertTrue("Pagina Home não encontrada", homePage.validar_pagina());

	}

	@Quando("Compro um item {string}")
	public void compro_um_item (String item) {
		homePage.selecionar_produto(item);
		Assert.assertTrue("Pagina Home não encontrada", homePage.validar_pagina_produto(item));
		homePage.clicar_comprar_produto();
		homePage.clicar_carrinho_compras();
		
		Assert.assertTrue("Pagina Carrinho de Compras não encontrada", carrinhoPage.validar_pagina_carrinho());
		Assert.assertTrue("Item Não encontrado no carrinho", carrinhoPage.validar_item_no_carrinho());
		carrinhoPage.clicar_checkout();
		
		Assert.assertTrue("Pagina Your Information não encontrada", yourInformationPage.validar_pagina_your_information());
		yourInformationPage.preencher_dados();
		yourInformationPage.clicar_continue();
		
		Assert.assertTrue("Pagina Overview não encontrada", overviewPage.validar_tela_overview());
		overviewPage.validar_itens_tela_overview();
		overviewPage.clicar_botao_finish();
	}

	@Então("Confirmo que compra foi efetuada")
	public void sou_autenticado_com_sucesso() throws InterruptedException {
		Assert.assertTrue("Pagina Finish não encontrada", finishPage.validar_tela_finish());
		finishPage.validar_itens_tela_finish();
	}

}
