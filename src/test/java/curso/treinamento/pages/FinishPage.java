package curso.treinamento.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import curso.treinamento.utils.Helper;
import org.junit.Assert;

public class FinishPage extends Helper {

	public FinishPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[text()='Finish']")
	private WebElement textoFinish;
	
	@FindBy(xpath = "//h2[text()='THANK YOU FOR YOUR ORDER']")
	private WebElement textoThankYou;
	
	@FindBy(xpath = "//img[@class='pony_express']")
	private WebElement imagemPonyExpress;
	
	public Boolean validar_tela_finish() {
		aguardar_elemento(10, textoFinish);
		screenshot("Tela Finish");
		return textoFinish.isDisplayed();
	}
	
	public void validar_itens_tela_finish() {
		Assert.assertTrue("Texto THANK YOU FOR YOUR ORDER não encontrado", textoThankYou.isDisplayed());
		Assert.assertTrue("Imagem Pony Express não encontrada", imagemPonyExpress.isDisplayed());
	}
}
