package curso.treinamento.setup;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

	public static WebDriver driver;
	public static ResourceBundle bundle = ResourceBundle.getBundle("project");
	
	public static String data;
	public static String time;
	public static Scenario cenario;

	@Before
	public void startTest(Scenario scenario) {

		System.setProperty("webdriver.chrome.driver", "src/test/resources/mac/chromedriver");
		driver = new ChromeDriver();
		
		driver.manage().window().maximize();

		data = getDate();
		time = getTime();
		cenario = scenario;
		

		driver.get(bundle.getString("env.url"));
	}

	@After
	public void tearDown(Scenario scenario) {
		driver.close();
	}

	public static WebDriver getDriver() {
		return driver;
	}
	
	public static String getDate() {
		String data = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
		return data;
	}
	
	public static String getTime() {
		String hora = new SimpleDateFormat("HH_mm_ss").format(Calendar.getInstance().getTime());
		return hora;
	}

}
